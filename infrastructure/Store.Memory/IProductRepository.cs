﻿using System.Collections.Generic;

namespace Store.Memory
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetAllByName(string namePart);
    }
}