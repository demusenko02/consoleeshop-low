﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Memory
{
    public class UserRepository : IUserRepository
    {
        public static UserRepository Instance { get; } = new();
        private int _idLast;
        private readonly List<User> _registeredUsers;
        private UserRepository()
        {
            _registeredUsers = new List<User>();
            Create(new User("admin1", "admin", "Alex", "admin1@gmail.com", UserRole.Administrator));
            Create(new User("admin2", "admin", "John", "admin2@gmail.com", UserRole.Administrator));
            Create(new User("admin3", "admin", "Fred", "admin3@gmail.com", UserRole.Administrator));
            Create(new User("user1", "user1", "Max", "user1@gmail.com", UserRole.RegisteredUser));
            Create(new User("user2", "user2", "Ivan", "user2@gmail.com", UserRole.RegisteredUser));
            Create(new User("user3", "user3", "James", "user3@gmail.com", UserRole.RegisteredUser));
            Create(new User("user4", "user4", "Oliver", "user4@gmail.com", UserRole.RegisteredUser));
            Create(new User("user5", "user5", "William", "user5@gmail.com", UserRole.RegisteredUser));
            Create(new User("user6", "user6", "Ethan", "user6@gmail.com", UserRole.RegisteredUser));
        }
        public User? GetByLogin(string login)
        {
            return _registeredUsers.Find(user => user.LogIn == login);
        }

        public User? GetByEmail(string email)
        {
            return _registeredUsers.Find(user => user.Email == email);
        }

        public IEnumerable<User> GetAllByFilter(Func<User, bool>? predicate = null)
        {
            return predicate == null ? _registeredUsers : _registeredUsers.Where(predicate);
        }
        public User? GetById(int id)
        {
            return _registeredUsers.Find(user => user.Id == id);
        }
        public void Create(User item)
        {
            item.Id = ++_idLast;
            _registeredUsers.Add(item);
        }
        public void Delete(User item)
        {
            _registeredUsers.Remove(item);
        }
        public void Update(User item)
        {
            int userIndex = _registeredUsers.FindIndex(user => user.Id == item.Id);
            if (userIndex == -1) return;
            _registeredUsers[userIndex] = item;
        }
    }
}