﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Memory
{
    public class AdminRepository : IAdminRepository
    {
        public static AdminRepository Instance { get; } = new();
        private readonly List<Administrator> _administrators;
        private int _idLast;
        public IEnumerable<Administrator> GetAllByFilter(Func<Administrator, bool>? predicate = null)
        {
            return predicate == null ? _administrators : _administrators.Where(predicate);
        }
        private AdminRepository()
        {
            _administrators = new List<Administrator>();
            Create(new Administrator(1));
            Create(new Administrator(2));
            Create(new Administrator(9));
        }
        public Administrator? GetById(int id)
        {
            return _administrators.Find(user => user.Id == id);
        }
        public Administrator? GetByUserId(int userId)
        {
            return _administrators.Find(user => user.UserId == userId);
        }
        public void Create(Administrator item)
        {
            item.Id = ++_idLast;
            _administrators.Add(item);
        }
        public void Delete(Administrator item)
        {
            _administrators.Remove(item);
        }
        public void Update(Administrator item)
        {
            int adminIndex = _administrators.FindIndex(user => user.Id == item.Id);
            if (adminIndex == -1) return;
            _administrators[adminIndex] = item;
        }
    }
}