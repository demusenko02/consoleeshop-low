﻿using System;
using System.Collections.Generic;

namespace Store.Memory
{

    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> GetAllByFilter(Func<T, bool>? predicate = null);
        T? GetById(int id);
        void Create(T item);
        void Delete(T item);
        void Update(T item);
    }
}