﻿namespace Store.Memory
{
    public interface IUserRepository : IRepository<User>
    {
        User? GetByLogin(string login);
        User? GetByEmail(string email);
    }
}