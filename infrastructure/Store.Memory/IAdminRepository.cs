﻿namespace Store.Memory
{
    public interface IAdminRepository:IRepository<Administrator>
    {
        Administrator? GetByUserId(int id);
    }
}