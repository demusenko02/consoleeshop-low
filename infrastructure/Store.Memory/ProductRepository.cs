﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Memory
{
    public class ProductRepository : IProductRepository
    {
        public static ProductRepository Instance { get; } = new();
        private int _idLast;
        private readonly List<Product> _products;
        private ProductRepository()
        {
            _products = new List<Product>();
            Create(new Product("prod1", "desc1", 1));
            Create(new Product("prod2", "desc2", 10));
            Create(new Product("prod3", "desc3", 100));
            Create(new Product("prod4", "desc4", 1000));
            Create(new Product("prod5", "desc5", 10000));
        }
        public IEnumerable<Product> GetAllByName(string namePart)
        {
            return _products.Where(product => product.Name.Contains(namePart, StringComparison.InvariantCultureIgnoreCase));
        }
        public IEnumerable<Product> GetAllByFilter(Func<Product, bool>? predicate = null)
        {
            return predicate == null ? _products : _products.Where(predicate);
        }
        public Product? GetById(int id)
        {
            return _products.Find(product => product.Id == id);
        }
        public void Create(Product item)
        {
            item.Id = ++_idLast;
            _products.Add(item);
        }
        public void Delete(Product item)
        {
            _products.Remove(item);
        }
        public void Update(Product item)
        {
            int productIndex = _products.FindIndex(product => product.Id == item.Id);
            if (productIndex == -1) return;
            _products[productIndex] = item;
        }
    }
}
