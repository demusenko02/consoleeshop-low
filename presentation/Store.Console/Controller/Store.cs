﻿using Store.MainProj.Commands;
using Store.MainProj.Pages;
using Store.MainProj.Utility;

namespace Store.MainProj.Controller
{
    public class Store
    {
        private Page _currentPage;

        public Store()
        {
            _currentPage = PageContainer.GetPageFactory(PageContainer.PageWelcome)
                .CreatePage(new DataPage(new Guest()));
        }

        public void MainLoop()
        {
            while (true)
            {
                ConsoleUtility.CustomClear();

                _currentPage.Draw();

                _currentPage = ConsoleUtility.CancelableReadLine(out string inputBuffer) ? 
                    _currentPage.Input(inputBuffer).Execute(_currentPage) 
                    : CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(_currentPage);

                //_currentPage = _currentPage.Input(Console.ReadLine() ?? string.Empty).Execute(_currentPage);
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}

