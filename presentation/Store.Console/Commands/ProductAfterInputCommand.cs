﻿using System.Xml.Linq;
using Store.MainProj.Exceptions;
using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class ProductAfterInputCommand : Command
    {
        public override Page Execute(Page param)
        {
            string toBeChanged = (string)param.Data.CustomRequest["toBeChanged"];
            string newValue = (string)param.Data.CustomRequest["newValue"];
            Product product = (Product)param.Data.CustomRequest["product"];
            ProductRepository rep = ProductRepository.Instance;

            switch (toBeChanged)
            {
                case "name":
                    product.Name = newValue;
                    rep.Update(product);
                    param.Data.Message = "Name updated successfully";
                    break;
                case "desc":
                    product.Desc = newValue;
                    rep.Update(product);
                    param.Data.Message = "Description updated successfully";
                    break;
                case "price":
                    if (int.TryParse(newValue, out int newPrice) && newPrice >= 0)
                    {
                        product.Price = newPrice;
                        rep.Update(product);
                        param.Data.Message = "Price updated successfully";
                        break;
                    }
                    else
                    {
                        param.Data.Message = "Price must be a positive numerical value";
                        return CommandContainer.GetCommand(CommandContainer.CommandRedraw).Execute(param);
                    }

                default:
                    throw new UnknownCommandException();
            }

            return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
        }

    }
}

