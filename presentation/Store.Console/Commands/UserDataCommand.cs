﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class UserDataCommand : Command
    {
        public override Page Execute(Page param)
        {
            return PageContainer.GetPageFactory(PageContainer.PageUserData).CreatePage(param.Data);
        }
    }
}