﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChangeLoginCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest["toBeChanged"] = "login";
            return PageContainer.GetPageFactory(PageContainer.PageChangeData).CreatePage(param.Data);
        }

    }
}