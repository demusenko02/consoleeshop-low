﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class AuthorizeLogoutCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.Session.Clear();
            param.Data.Session["user"] = new Guest();
            param.Data.Message = "Logout successful";
            return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
        }
    }
}