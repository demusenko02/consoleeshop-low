﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChangeNameCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest["toBeChanged"] = "name";
            return PageContainer.GetPageFactory(PageContainer.PageChangeData).CreatePage(param.Data);
        }
    }
}