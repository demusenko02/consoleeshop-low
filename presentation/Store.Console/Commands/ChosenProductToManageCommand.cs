﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChosenProductToManageCommand : Command
    {
        public override Page Execute(Page param)
        {
            return PageContainer.GetPageFactory(PageContainer.PageManageChosenProduct).CreatePage(param.Data);
        }
    }
}