﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class AuthorizeConfirmCommand : Command
    {
        public override Page Execute(Page param)
        {
            string login = (string)param.Data.CustomRequest["login"];
            string password = (string)param.Data.CustomRequest["password"];
            param.Data.CustomRequest.Clear();

            UserRepository repository = UserRepository.Instance;
            User? user = repository.GetByLogin(login);

            if (user == null || user.Password != password)
            {
                param.Data.Message = "Invalid password";
                return CommandContainer.GetCommand(CommandContainer.CommandRedraw).Execute(param);
            }

            param.Data.Session.Clear();
            param.Data.Session["user"] = user;

            param.Data.Message = "Authorization successful";

            return PageContainer.GetPageFactory(PageContainer.PageWelcome).CreatePage(param.Data);
        }
    }
}