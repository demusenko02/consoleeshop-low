﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class SignLoginCommand : Command
    {
        public override Page Execute(Page param)
        {
            return PageContainer.GetPageFactory(PageContainer.PageSignup).CreatePage(param.Data);
        }
    }
}