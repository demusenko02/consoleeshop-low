﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class AuthorizeLoginCommand : Command
    {
        public override Page Execute(Page param)
        {
            return PageContainer.GetPageFactory(PageContainer.PageLogin).CreatePage(param.Data);
        }
        
    }
}