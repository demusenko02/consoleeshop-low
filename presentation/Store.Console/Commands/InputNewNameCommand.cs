﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class InputNewNameCommand : Command
    {
        public override Page Execute(Page param)
        {
            string newName = (string)param.Data.CustomRequest["newValue"];
            User user = (User)param.Data.Session["user"];

            if (user.Name == newName)
            {
                param.Data.Message = "Nothing to change. Your old name matches the new.";
            }
            else
            {
                UserRepository repository = UserRepository.Instance;

                user.Name = newName;
                repository.Update(user);

                param.Data.Message = "Name has been changed.";
            }
           

            return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
        }
    }
}