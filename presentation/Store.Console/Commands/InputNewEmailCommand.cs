﻿using Store.MainProj.Pages;
using Store.MainProj.Utility;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class InputNewEmailCommand : Command
    {
        public override Page Execute(Page param)
        {


            string newEmail = (string)param.Data.CustomRequest["newValue"];
            User user = (User)param.Data.Session["user"];


            UserRepository repository = UserRepository.Instance;

            if (user.Email == newEmail)
            {
                param.Data.Message = "Nothing to change. Your old email matches the new.";
                return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
            }

            if (repository.GetByEmail(newEmail) != null)
            {
                param.Data.Message = "Email must be unique.";
                return CommandContainer.GetCommand(CommandContainer.CommandRedraw).Execute(param);
            }

            if (new EmailRegexHelper().IsEmail(newEmail) == false)
            {
                param.Data.Message = "Invalid email.";
                return CommandContainer.GetCommand(CommandContainer.CommandRedraw).Execute(param);
            }

            user.Email = newEmail;
            repository.Update(user);

            param.Data.Message = "Email has been changed.";

            return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
        }
    }
}