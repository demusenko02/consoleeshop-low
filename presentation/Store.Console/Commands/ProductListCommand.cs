﻿using System;
using System.Linq;
using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class ProductListCommand : Command
    {
        public override Page Execute(Page param)
        {
            ProductRepository rep = ProductRepository.Instance;

            Func<Product, bool>? predicate;

            if (param.Data.CustomRequest.TryGetValue("productName", out object? namePart))
            {
                predicate = product => product.Name.Contains((string)namePart, StringComparison.InvariantCultureIgnoreCase);
            }
            else
            {
                predicate = null;
            }
            //Data 

            //"User" - User
            //"list" - Product[]
            
            param.Data.CustomRequest["list"] = rep.GetAllByFilter(predicate).ToArray();

            return PageContainer.GetPageFactory(PageContainer.PageProductsList).CreatePage(param.Data);
        }
    }
}