﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class WelcomeCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest.Clear();
            return PageContainer.GetPageFactory(PageContainer.PageWelcome).CreatePage(param.Data);
        }
        
    }
}
