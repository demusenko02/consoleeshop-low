﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class AddNewProductCommand : Command
    {
        public override Page Execute(Page param)
        {
            if (param.Data.CustomRequest.ContainsKey("name") &&
                param.Data.CustomRequest.ContainsKey("desc") &&
                param.Data.CustomRequest.TryGetValue("price", out object? price))
            {

                if (int.TryParse((string)price, out int priceInt) && priceInt >= 0)
                {
                    ProductRepository rep = ProductRepository.Instance;
                    rep.Create(new Product((string)param.Data.CustomRequest["name"], (string)param.Data.CustomRequest["desc"], priceInt));
                    param.Data.Message = "Product added successfully";
                    return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
                }

                param.Data.Message = "Invalid price.";
                param.Data.CustomRequest.Remove("price");

            }
            return PageContainer.GetPageFactory(PageContainer.PageAddNewProduct).CreatePage(param.Data);
        }
    }
    
}