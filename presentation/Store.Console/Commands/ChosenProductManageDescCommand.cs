﻿using System.Runtime.CompilerServices;
using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChosenProductManageDescCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest["toBeChanged"] = "desc";
            return PageContainer.GetPageFactory(PageContainer.PageManageProductInput).CreatePage(param.Data);
        }

    }
}
