﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class RedrawCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.ClearActions();
            return param;
        }
        
    }
}