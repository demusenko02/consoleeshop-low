﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class SignPasswordCommand : Command
    {
        public override Page Execute(Page param)
        {
            string login = (string)param.Data.CustomRequest["login"];
            UserRepository repository = UserRepository.Instance;
            User? user = repository.GetByLogin(login);

            if (string.IsNullOrWhiteSpace(login))
            {
                param.Data.Message = "Login cannot be empty";
                param.Data.CustomRequest.Clear();
            }
            if (user != null)
            {
                param.Data.Message = "Login must be unique";
                param.Data.CustomRequest.Clear();
            }
            
            return PageContainer.GetPageFactory(PageContainer.PageSignup).CreatePage(param.Data);
        }
    }
}
//TODO: email check
//TODO: email 