﻿using System.Collections.Generic;
using System.Data;
using Store.MainProj.Exceptions;

namespace Store.MainProj.Commands
{
    public static class CommandContainer
    {
        private static readonly Dictionary<string, Command> Pages;

        public const string CommandLogout = "authLogout";

        public const string CommandAuthLogin = "authLogin";
        public const string CommandAuthPassword = "authPassword";
        public const string CommandAuthConfirm = "authConfirm";

        public const string CommandSignLogin = "signLogin";
        public const string CommandSignPassword = "signPassword";
        public const string CommandSignConfirm = "signConfirm";

        public const string CommandUserData = "userData";
        public const string CommandChangeLogin = "changeLogin";
        public const string CommandChangeName = "changeName";
        public const string CommandChangeEmail = "changeEmail";
        public const string CommandInputNewLogin = "inputNewLogin";
        public const string CommandInputNewName = "inputNewName";
        public const string CommandInputNewEmail = "inputNewEmail";

        public const string CommandProdList = "prodList";
        public const string CommandProdSearch = "prodSearch";
        public const string CommandCheckOrders = "checkOrders";
        public const string CommandConfirmOrder = "confirmOrder";
        public const string CommandChosenProductToManage = "chosenProductToManage";

        public const string CommandManageUsers = "manageUsers";
        public const string CommandManageProducts = "manageProducts";
        public const string CommandManageOrders = "manageOrders";
        public const string CommandRedraw = "redraw";

        public const string CommandChosenProductManageName = "productManageName";
        public const string CommandChosenProductManageDesc = "productManageDesc";
        public const string CommandChosenProductManagePrice = "productManagePrice";
        public const string CommandProductAfterInput = "productAfterInput";
        public const string CommandChosenProductRemove = "chosenProductRemove";
        public const string CommandAddNewProduct = "addNewProduct";

        public const string CommandWelcome = "welcomeCmd";

        static CommandContainer()
        {
            Pages = new Dictionary<string, Command>
            {
                { CommandLogout, new AuthorizeLogoutCommand()},

                { CommandAuthLogin, new AuthorizeLoginCommand()},
                { CommandAuthPassword, new AuthorizePasswordCommand()},
                { CommandAuthConfirm, new AuthorizeConfirmCommand()},

                { CommandSignLogin, new SignLoginCommand()},
                { CommandSignPassword, new SignPasswordCommand()},
                { CommandSignConfirm, new SignConfirmCommand()},

                { CommandUserData, new UserDataCommand()},
                { CommandChangeLogin, new ChangeLoginCommand()},
                { CommandChangeName, new ChangeNameCommand()},
                { CommandChangeEmail, new ChangeEmailCommand()},
                { CommandInputNewLogin, new InputNewLoginCommand()},
                { CommandInputNewName, new InputNewNameCommand()},
                { CommandInputNewEmail, new InputNewEmailCommand()},

                { CommandProdList, new ProductListCommand()},
                { CommandProdSearch, new ProductSearchCommand()},
                { CommandCheckOrders, new UserOrdersCommand()},
                { CommandConfirmOrder, new UserConfirmOrderCommand()},
                { CommandChosenProductToManage, new ChosenProductToManageCommand()},

                { CommandManageUsers, new AdminManageUsersCommand()},
                { CommandManageProducts, new AdminManageProductsCommand()},
                { CommandManageOrders, new AdminManageOrdersCommand()},
                { CommandRedraw, new RedrawCommand()},
                { CommandWelcome, new WelcomeCommand()},
                { CommandChosenProductManageName, new ChosenProductManageNameCommand()},
                { CommandChosenProductManageDesc, new ChosenProductManageDescCommand()},
                { CommandChosenProductManagePrice, new ChosenProductManagePriceCommand()},
                { CommandProductAfterInput, new ProductAfterInputCommand()},
                { CommandChosenProductRemove, new ChosenProductRemoveCommand() },
                { CommandAddNewProduct, new AddNewProductCommand() },
            };
        }

        public static Command GetCommand(string commandName)
        {
            if (!Pages.TryGetValue(commandName, out Command? res)) throw new UnknownCommandException(commandName);
            return res;
        }
    }
}