﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChangeEmailCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest["toBeChanged"] = "email";
            return PageContainer.GetPageFactory(PageContainer.PageChangeData).CreatePage(param.Data);
        }
    }
}