﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class AuthorizePasswordCommand : Command
    {
        public override Page Execute(Page param)
        {
            string login = (string)param.Data.CustomRequest["login"];
            UserRepository repository = UserRepository.Instance;
            User? user = repository.GetByLogin(login);
            if (user == null)
            {
                param.Data.Message = "User with this login doesn't exist";
                param.Data.CustomRequest.Clear();
            }

            return PageContainer.GetPageFactory(PageContainer.PageLogin).CreatePage(param.Data);
        }
    }
}