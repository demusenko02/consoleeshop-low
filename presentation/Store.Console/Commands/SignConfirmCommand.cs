﻿using Store.MainProj.Exceptions;
using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class SignConfirmCommand : Command
    {
        public override Page Execute(Page param)
        {
            string login = (string)param.Data.CustomRequest["login"];
            string password = (string)param.Data.CustomRequest["password"];
            param.Data.CustomRequest.Clear();

            UserRepository repository = UserRepository.Instance;
            if (repository.GetByLogin(login) != null || password.Length == 0)
            {
                param.Data.Message = "Password cannot be empty";
                return CommandContainer.GetCommand(CommandContainer.CommandRedraw).Execute(param);
            }

            repository.Create(new User(login, password, string.Empty, string.Empty, UserRole.RegisteredUser));

            param.Data.Session.Clear();
            param.Data.Session["user"] = repository.GetByLogin(login) ?? throw new UnknownCommandException();
            param.Data.Message = "Registration successful";
            return PageContainer.GetPageFactory(PageContainer.PageWelcome).CreatePage(param.Data);
        }
    }
}