﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public abstract class Command
    {
        public abstract Page Execute(Page param);
    }
}