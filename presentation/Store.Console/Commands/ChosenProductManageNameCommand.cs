﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChosenProductManageNameCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest["toBeChanged"] = "name";
            return PageContainer.GetPageFactory(PageContainer.PageManageProductInput).CreatePage(param.Data);
        }

    }
}