﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class ChosenProductRemoveCommand : Command
    {
        public override Page Execute(Page param)
        {
            Product product = (Product)param.Data.CustomRequest["product"];
            ProductRepository rep = ProductRepository.Instance;
            rep.Delete(product);
            param.Data.Message = "Removal successful";
            return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
        }
    }
}