﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ChosenProductManagePriceCommand : Command
    {
        public override Page Execute(Page param)
        {
            param.Data.CustomRequest["toBeChanged"] = "price";
            return PageContainer.GetPageFactory(PageContainer.PageManageProductInput).CreatePage(param.Data);
        }

    }
}