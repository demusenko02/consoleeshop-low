﻿using System.Linq;
using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class AdminManageProductsCommand : Command
    {
        public override Page Execute(Page param)
        {
            ProductRepository rep = ProductRepository.Instance;

            param.Data.CustomRequest["list"] = rep.GetAllByFilter().ToArray();

            return PageContainer.GetPageFactory(PageContainer.PageManageProductList).CreatePage(param.Data);
        }
    }
}