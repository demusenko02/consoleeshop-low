﻿using Store.MainProj.Pages;
using Store.Memory;

namespace Store.MainProj.Commands
{
    public class InputNewLoginCommand : Command
    {
        public override Page Execute(Page param)
        {
            string newLogin = (string)param.Data.CustomRequest["newValue"];
            User user = (User)param.Data.Session["user"];


            UserRepository repository = UserRepository.Instance;

            if (user.LogIn == newLogin)
            {
                param.Data.Message = "Nothing to change. Your old login matches the new.";
                return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
            }

            if (repository.GetByLogin(newLogin) != null)
            {
                param.Data.Message = "Login must be unique.";
                return CommandContainer.GetCommand(CommandContainer.CommandRedraw).Execute(param);
            }

            user.LogIn = newLogin;
            repository.Update(user);

            param.Data.Message = "Login has been changed.";

            return CommandContainer.GetCommand(CommandContainer.CommandWelcome).Execute(param);
        }
    }
}