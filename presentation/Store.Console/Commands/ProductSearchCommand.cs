﻿using Store.MainProj.Pages;

namespace Store.MainProj.Commands
{
    public class ProductSearchCommand : Command
    {
        public override Page Execute(Page param)
        {
            return PageContainer.GetPageFactory(PageContainer.PageProductSearch).CreatePage(param.Data);
        }
    }
}