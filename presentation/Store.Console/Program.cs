﻿using System;
using System.Text;

namespace Store.MainProj
{
    internal class Program
    {
        private static void Main()
        {
            Console.InputEncoding = Console.OutputEncoding = Encoding.Unicode;

            Console.ForegroundColor = ConsoleColor.White;
            new Controller.Store().MainLoop();
        }
    }
}
