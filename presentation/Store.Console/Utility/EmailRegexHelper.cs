﻿using System.Text.RegularExpressions;

namespace Store.MainProj.Utility
{
    public class EmailRegexHelper
    {
        private readonly Regex _pattern = new(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
        public bool IsEmail(string email)
        {
            return _pattern.IsMatch(email);
        }

    }
}