﻿using System;
using System.Runtime.Serialization;

namespace Store.MainProj.Exceptions
{
    [Serializable]
    public class PageException : Exception
    {
        public PageException() { }
        public PageException(string message)
            : base(message) { }
        public PageException(string message, Exception innerException)
            : base(message, innerException) { }
        protected PageException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}