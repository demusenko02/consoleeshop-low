﻿using System;
using System.Runtime.Serialization;

namespace Store.MainProj.Exceptions
{
    [Serializable]
    public class UnknownCommandException : Exception
    {
        public UnknownCommandException() { }
        public UnknownCommandException(string message)
            : base(message) { }
        public UnknownCommandException(string message, Exception innerException)
            : base(message, innerException) { }

        protected UnknownCommandException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}