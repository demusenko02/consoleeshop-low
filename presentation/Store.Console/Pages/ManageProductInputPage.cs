﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class ManageProductInputPage : Page
    {
        protected override void DrawInternal()
        {
            Console.WriteLine("Input new value:");
        }
        public override Command Input(string str)
        {
            Data.CustomRequest["newValue"] = str;
            return CommandContainer.GetCommand(CommandContainer.CommandProductAfterInput);
        }
        public ManageProductInputPage(DataPage args) : base(args) { }
    }
    public class AddNewProductPage : Page
    {
        protected override void DrawInternal()
        {
            Console.WriteLine("Product name:");
            if (!Data.CustomRequest.TryGetValue("name", out object? name)) return;

            Console.WriteLine((string)name);
            Console.WriteLine("Product description:");
            if (!Data.CustomRequest.TryGetValue("desc", out object? desc)) return;

            Console.WriteLine((string)desc);
            Console.WriteLine("Product price:");
        }
        public override Command Input(string str)
        {
            if (!Data.CustomRequest.ContainsKey("name"))
            {
                Data.CustomRequest["name"] = str;
            }
            else if (!Data.CustomRequest.ContainsKey("desc"))
            {
                Data.CustomRequest["desc"] = str;
            }
            else
            {
                Data.CustomRequest["price"] = str;
            }
            
            return CommandContainer.GetCommand(CommandContainer.CommandAddNewProduct);
        }
        public AddNewProductPage(DataPage args) : base(args) { }
    }
}