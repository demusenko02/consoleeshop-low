﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class SearchPage : Page
    {
        public SearchPage(DataPage args) : base(args) { }
        protected override void DrawInternal()
        {
            Console.WriteLine("Input product name or part of name: ");
        }

        public override Command Input(string str)
        {
            Data.CustomRequest["productName"] = str;

            return CommandContainer.GetCommand(CommandContainer.CommandProdList);
        }
    }
}