﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class ProductListPage : Page
    {
        public ProductListPage(DataPage args) : base(args) { }
        protected override void DrawInternal()
        {
            Product[] list = (Product[])Data.CustomRequest["list"];
            if (list.Length == 0)
            {
                Console.WriteLine("No products found by filter");
            }
            else foreach (var item in list)
            {
                Console.WriteLine($"Product: {item.Name}");
                Console.WriteLine($"Description: {item.Desc}");
                Console.WriteLine($"Price: {item.Price}");
                Console.WriteLine();
            }
            Console.WriteLine("Press ESC");
        }

        public override Command Input(string str)
        {
            return CommandContainer.GetCommand(CommandContainer.CommandRedraw);
        }
    }
}