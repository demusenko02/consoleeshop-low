﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class ManageChosenProductPage : Page
    {
        public ManageChosenProductPage(DataPage args) : base(args) { }
        protected override void DrawInternal()
        {
            Product chosenProduct = (Product)Data.CustomRequest["product"];

            int counter = 0;

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChosenProductManageName));
            Console.WriteLine($"{counter}. Name: {chosenProduct.Name}");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChosenProductManageDesc));
            Console.WriteLine($"{counter}. Desc: {chosenProduct.Desc}");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChosenProductManagePrice));
            Console.WriteLine($"{counter}. Price: {chosenProduct.Price}");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChosenProductRemove));
            Console.WriteLine($"{counter}. Remove product.\n");
        }

        public override Command Input(string str)
        {
            if (Actions.TryGetValue(str, out Command? cmd))
            {
                return cmd;
            }
            Data.Message = "Invalid input";
            return CommandContainer.GetCommand(CommandContainer.CommandRedraw);
        }
    }
}