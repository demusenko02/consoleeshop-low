﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class UserDataPage : Page
    {
        public UserDataPage(DataPage args) : base(args) { }
        protected override void DrawInternal()
        {
            User user = (User)Data.Session["user"];

            int counter = 0;

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChangeLogin));
            Console.WriteLine($"({counter}) Your login:\t{user.LogIn}");
            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChangeName));
            Console.WriteLine($"({counter}) Your name:\t{user.Name}");
            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChangeEmail));
            Console.WriteLine($"({counter}) Your email:\t{user.Email}");

            Console.WriteLine("Input number to change your personal data or press ESC to go back");
        }

        public override Command Input(string str)
        {
            return Actions.TryGetValue(str, out Command? cmd) ? cmd : CommandContainer.GetCommand(CommandContainer.CommandRedraw);
        }
    }
}
