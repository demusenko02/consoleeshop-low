﻿using System.Collections.Generic;

namespace Store.MainProj.Pages
{
    public class DataPage
    {
        public DataPage(User user)
        {
            Session.Add("user", user);
        }
        public Dictionary<string, object> CustomRequest { get; set; } = new();
        public Dictionary<string, object> Session { get; set; } = new();
        public string Message { get; set; } = string.Empty;
    }
}