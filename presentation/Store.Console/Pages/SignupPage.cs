﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class SignupPage : Page
    {
        public SignupPage(DataPage args) : base(args){ }
        protected override void DrawInternal()
        {

            Console.WriteLine("Login: ");
            if (!Data.CustomRequest.TryGetValue("login", out object? login)) return;

            Console.WriteLine((string)login);
            Console.WriteLine("Password: ");
        }

        public override Command Input(string str)
        {
            if (Data.CustomRequest.ContainsKey("login"))
            {
                Data.CustomRequest["password"] = str;
                return CommandContainer.GetCommand(CommandContainer.CommandSignConfirm);
            }

            Data.CustomRequest["login"] = str;
            return CommandContainer.GetCommand(CommandContainer.CommandSignPassword);
        }
    }
}