﻿using System;
using System.Net.Http;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class ManageProductListPage : Page
    {
        public ManageProductListPage(DataPage args) : base(args) { }
        protected override void DrawInternal()
        {
            Product[] allProducts = (Product[])Data.CustomRequest["list"];

            Console.WriteLine("Choose product to manage:\n");
            int i;
            for (i = 0; i < allProducts.Length; i++)
            {
                Actions.Add((i + 1).ToString(), CommandContainer.GetCommand(CommandContainer.CommandChosenProductToManage));
                Console.WriteLine($"{i + 1}. {allProducts[i].Name}");
                Console.WriteLine($"Desc: {allProducts[i].Desc}");
                Console.WriteLine($"Price: {allProducts[i].Price}\n");
            }
            Actions.Add(0.ToString(), CommandContainer.GetCommand(CommandContainer.CommandAddNewProduct));
            Console.WriteLine($"0. Add new product");
        }

        public override Command Input(string str)
        {
            if (Actions.TryGetValue(str, out Command? cmd))
            {
                if (str != "0")
                {
                    Data.CustomRequest["product"] = ((Product[])Data.CustomRequest["list"])[int.Parse(str) - 1];
                }
                return cmd;
            }

            Data.Message = "Invalid input";
            return CommandContainer.GetCommand(CommandContainer.CommandRedraw);
        }
    }
}
