﻿namespace Store.MainProj.Pages.Factory
{
    public class ProductListFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new ProductListPage(args);
        }
    }
}