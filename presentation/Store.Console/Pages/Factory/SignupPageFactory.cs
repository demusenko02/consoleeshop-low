﻿namespace Store.MainProj.Pages.Factory
{
    public class SignupPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new SignupPage(args);
        }
    }
}