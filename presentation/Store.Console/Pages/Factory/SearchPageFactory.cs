﻿namespace Store.MainProj.Pages.Factory
{
    public class SearchPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new SearchPage(args);
        }
    }
}