﻿namespace Store.MainProj.Pages.Factory
{
    public class ManageChosenProductPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new ManageChosenProductPage(args);
        }
    }
}