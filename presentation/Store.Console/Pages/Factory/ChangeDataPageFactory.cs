﻿namespace Store.MainProj.Pages.Factory
{
    public class ChangeDataPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new UserChangeDataPage(args);
        }
    }
}
