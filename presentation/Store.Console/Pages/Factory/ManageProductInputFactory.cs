﻿namespace Store.MainProj.Pages.Factory
{
    public class ManageProductInputFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new ManageProductInputPage(args);
        }
    }
    public class AddNewProductPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new AddNewProductPage(args);
        }
    }
}