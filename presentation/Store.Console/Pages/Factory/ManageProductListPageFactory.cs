﻿namespace Store.MainProj.Pages.Factory
{
    public class ManageProductListPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new ManageProductListPage(args);
        }
    }
}
