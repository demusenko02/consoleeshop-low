﻿namespace Store.MainProj.Pages.Factory
{
    public class WelcomePageFactory:IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new WelcomePage(args);
        }
    }
}