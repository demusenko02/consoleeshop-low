﻿namespace Store.MainProj.Pages.Factory
{
    public class AuthPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new AuthPage(args);
        }
    }
}