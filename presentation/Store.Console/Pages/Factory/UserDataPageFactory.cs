﻿namespace Store.MainProj.Pages.Factory
{
    public class UserDataPageFactory : IPageFactory
    {
        public Page CreatePage(DataPage args)
        {
            return new UserDataPage(args);
        }
    }
}
