﻿namespace Store.MainProj.Pages.Factory
{
    public interface IPageFactory
    {
        Page CreatePage(DataPage args);
    }
}