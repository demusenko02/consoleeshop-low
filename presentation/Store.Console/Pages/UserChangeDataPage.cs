﻿using System;
using Store.MainProj.Commands;
using Store.MainProj.Exceptions;

namespace Store.MainProj.Pages
{
    public class UserChangeDataPage : Page
    {
        public UserChangeDataPage(DataPage args) : base(args) { }
        protected override void DrawInternal()
        {
            string toBeChanged = (string)Data.CustomRequest["toBeChanged"];

            Console.WriteLine($"New {toBeChanged}:");

        }

        public override Command Input(string str)
        {
            string toBeChanged = (string)Data.CustomRequest["toBeChanged"];
            Data.CustomRequest["newValue"] = str;

            return toBeChanged switch
            {
                "login" => CommandContainer.GetCommand(CommandContainer.CommandInputNewLogin),
                "name" => CommandContainer.GetCommand(CommandContainer.CommandInputNewName),
                "email" => CommandContainer.GetCommand(CommandContainer.CommandInputNewEmail),
                _ => throw new PageException($"UserChangeDataPage unknown toBeChanged parameter: {toBeChanged}")
            };
        }
    }
}
