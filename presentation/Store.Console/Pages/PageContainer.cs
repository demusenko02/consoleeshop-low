﻿using System.Collections.Generic;
using Store.MainProj.Exceptions;
using Store.MainProj.Pages.Factory;

namespace Store.MainProj.Pages
{
    public static class PageContainer
    {
        private static readonly Dictionary<string, IPageFactory> Pages;

        public const string PageWelcome = "welcome";
        public const string PageLogin = "authLogin";
        public const string PageSignup = "authSignup";
        public const string PageProductsList = "productList";
        public const string PageProductSearch = "productSearch";
        public const string PageUserData = "userData";
        public const string PageChangeData = "changeData";
        public const string PageManageProductList = "manageProductList";
        public const string PageManageChosenProduct = "manageChosenProduct";
        public const string PageManageProductInput = "manageProductInput";
        public const string PageAddNewProduct = "addNewProduct";

        static PageContainer()
        {
            Pages = new Dictionary<string, IPageFactory>
            {
                {PageWelcome, new WelcomePageFactory()},
                {PageLogin, new AuthPageFactory()},
                {PageSignup, new SignupPageFactory()},
                {PageProductsList, new ProductListFactory()},
                {PageProductSearch, new SearchPageFactory()},
                {PageUserData, new UserDataPageFactory()},
                {PageChangeData, new ChangeDataPageFactory()},
                {PageManageProductList, new ManageProductListPageFactory() },
                {PageManageChosenProduct, new ManageChosenProductPageFactory() },
                {PageManageProductInput, new ManageProductInputFactory() },
                {PageAddNewProduct, new AddNewProductPageFactory()},
            };
        }

        public static IPageFactory GetPageFactory(string pageName)
        {
            if (!Pages.TryGetValue(pageName, out IPageFactory? res)) throw new PageException(pageName);
            return res;
        }
    }
}