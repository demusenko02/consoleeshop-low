﻿using System;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public class WelcomePage : Page
    {
        protected override void DrawInternal()
        {
            User user = (User)Data.Session["user"];

            Console.WriteLine("Welcome!");
            Console.WriteLine($"Your role - {Enum.GetName(user.Role)}");
            Console.WriteLine();

            int counter = 0;

            if (user.Role > UserRole.Guest)
            {
                Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandLogout));
                Console.WriteLine($"{counter}. Log out");
            }
            else
            {
                Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandAuthLogin));
                Console.WriteLine($"{counter}. Log in");

                Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandSignLogin));
                Console.WriteLine($"{counter}. Sign up");
            }

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandProdList));
            Console.WriteLine($"{counter}. Show all products");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandProdSearch));
            Console.WriteLine($"{counter}. Search products");

            if (user.Role == UserRole.Guest) return;

            Console.ForegroundColor = ConsoleColor.Green;

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandCheckOrders));
            Console.WriteLine($"{counter}. Show your orders");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandUserData));
            Console.WriteLine($"{counter}. Change your user data");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandConfirmOrder));
            Console.WriteLine($"{counter}. Confirm or cancel your order");

            Console.ForegroundColor = ConsoleColor.White;

            if (user.Role != UserRole.Administrator) return;
            Console.ForegroundColor = ConsoleColor.Red;

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandManageUsers));
            Console.WriteLine($"{counter}. Manage customer's data");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandManageProducts));
            Console.WriteLine($"{counter}. Manage product list");

            Actions.Add((++counter).ToString(), CommandContainer.GetCommand(CommandContainer.CommandManageOrders));
            Console.WriteLine($"{counter}. Manage orders list");

            Console.ForegroundColor = ConsoleColor.White;
        }
        public override Command Input(string str)
        {
            if (Actions.TryGetValue(str, out Command? cmd))
            {
                return cmd;
            }

            Data.Message = "Invalid input";
            return CommandContainer.GetCommand(CommandContainer.CommandRedraw);
        }
        public WelcomePage(DataPage args) : base(args) { }
    }
}
