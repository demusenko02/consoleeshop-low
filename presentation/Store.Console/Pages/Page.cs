﻿using System;
using System.Collections.Generic;
using Store.MainProj.Commands;

namespace Store.MainProj.Pages
{
    public abstract class Page
    {
        public DataPage Data { get; }

        protected Dictionary<string, Command> Actions;
        public void ClearActions() => Actions.Clear();
        protected Page(DataPage args)
        {
            Data = args;
            Actions = new Dictionary<string, Command>();
        }
        protected abstract void DrawInternal();

        public void Draw()
        {
            if (!string.IsNullOrWhiteSpace(Data.Message))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{Data.Message}\n");
                Console.ForegroundColor = ConsoleColor.White;
                Data.Message = string.Empty;
            }
            DrawInternal();
        }
        public abstract Command Input(string str);
    }
}