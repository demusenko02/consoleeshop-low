﻿namespace Store
{
    public class Administrator : Entity
    {
        public Administrator(int userId)
        {
            Id = -1;
            UserId = userId;
        }
        public int UserId { get; set; }

    }
}