﻿namespace Store
{
    public abstract class Entity
    {
        public int Id { get; set; }
        protected Entity()
        {
        }
    }
}