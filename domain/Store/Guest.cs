﻿namespace Store
{
    public class Guest : User
    {
        public Guest()
        {
            Role = UserRole.Guest;
        }
    }
}