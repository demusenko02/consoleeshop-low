﻿namespace Store
{
    public class Product : Entity
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Price { get; set; }
        public Product(string name, string desc, int price)
        {
            Id = -1;
            Name = name;
            Desc = desc;
            Price = price;
        }

    }
}
