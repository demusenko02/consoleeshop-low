﻿namespace Store
{
    public class User : Entity
    {
        public string LogIn { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public UserRole Role { get; set; }
        public User(string logIn, string password, string name, string email, UserRole role)
        {
            Id = -1;
            LogIn = logIn;
            Password = password;
            Role = role;
            Name = name;
            Email = email;
        }
        protected User() { }
    }
}