﻿namespace Store
{
    public enum UserRole
    {
        Guest = 0,
        RegisteredUser,
        Administrator
    }

}